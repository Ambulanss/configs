set number
set relativenumber
set ruler
" this is for tabs to become 4 whitespaces 
set expandtab
set softtabstop=0
set tabstop=8
set shiftwidth=4
set smarttab
set clipboard+=unnamedplus

" this reads the file if it was changed from the outside
set autoread

set hlsearch
set incsearch

set showmatch
set mat=2

set noerrorbells
set novisualbell
set termguicolors

syntax enable

set mouse=a
let g:NERDTreeMouseMode=3

" whatever you yank will be in the system's clipboard
set clipboard+=unnamedplus

" plugin time
call plug#begin(stdpath('data') . '/plugged')

" cool theme
Plug 'morhetz/gruvbox'
" for better completion
Plug 'neoclide/coc.nvim', {'branch': 'release'}

call plug#end()

colorscheme gruvbox


" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
nmap <silent> K :call <SID>show_documentation()<CR>

function s:show_documentation()
    if (index(['vim', 'help'], &filetype) >= 0)
        execute 'h '.expand('<cword>')
    else
        call CocAction('doHover')
    endif
endfunction

nmap <F2> <Plug>(coc-rename)
