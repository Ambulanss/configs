# GTK
export CLUTTER_BACKEND=wayland
#export GDK_BACKEND=wayland             # this can prevent programs from starting (e.g. chromium and electron apps). therefore, this should be set per app instead of globally.
export MOZ_ENABLE_WAYLAND=1             # only start firefox in wayland mode and no other GTK apps

# Qt
# XDG_CURRENT_DESKTOP=GNOME
export XDG_SESSION_TYPE=wayland
export QT_QPA_PLATFORM=wayland-egl
export QT_QPA_PLATFORMTHEME="qt5ct"
# QT_STYLE_OVERRIDE=GTK
export QT_WAYLAND_FORCE_DPI=physical
export QT_WAYLAND_DISABLE_WINDOWDECORATION=1

# elementary
export ECORE_EVAS_ENGINE=wayland_egl
export ELM_ENGINE=wayland_egl
export ELM_DISPLAY=wl
export ELM_ACCEL=gl

# clutter
export CLUTTER_BACKEND=wayland

# sdl
export SDL_VIDEODRIVER=wayland

# java
export _JAVA_AWT_WM_NONREPARENTING=1
