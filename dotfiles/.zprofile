# GTK
CLUTTER_BACKEND=wayland
#GDK_BACKEND=wayland             # this can prevent programs from starting (e.g. chromium and electron apps). therefore, this should be set per app instead of globally.
MOZ_ENABLE_WAYLAND=1             # only start firefox in wayland mode and no other GTK apps

# Qt
XDG_SESSION_TYPE=wayland
QT_QPA_PLATFORM=wayland
QT_QPA_PLATFORMTHEME=qt5ct
QT_WAYLAND_FORCE_DPI=physical
QT_WAYLAND_DISABLE_WINDOWDECORATION=1

# elementary
ECORE_EVAS_ENGINE=wayland_egl
ELM_ENGINE=wayland_egl
ELM_DISPLAY=wl
ELM_ACCEL=gl

# clutter
CLUTTER_BACKEND=wayland

# sdl
SDL_VIDEODRIVER=wayland

# java
_JAVA_AWT_WM_NONREPARENTING=1

if [ "$(tty)" = "/dev/tty1" ]; then
	exec sway
fi
